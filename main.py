# ================================================================

'''
# Author: LA Remmelzwaal
# Date: January 2020
# Adapted from: https://gitlab.com/ja4nm/python-neural-net/

Features added:

    >>> Minor bug fixes
    >>> 4 datasets available (xor, cifar10, mnist, fashion_mnist)
    >>> All outputs converted to categorical
    >>> Training progress shown
    >>> Accuracy score shown during training
    >>> K-fold training
'''

# ================================================================

dataset = "fashion_mnist"     # Select dataset: cifar10, mnist, fashion_mnist
method = "train"      # Method: train / load
kfolds = 5            # K-folds
input_dim = 16        # Image dimensions = input_dim * input_dim
epochs = 10           # Epochs
samples = 1000        # Total image samples used from datasets

# ================================================================

# imports
from NeuralNetwork import *
import numpy as np
from sklearn.model_selection import StratifiedKFold

# ================================================================

# setup dataset
if (dataset in ["mnist"]):
    x_train, y_train = NeuralNetwork.load_dataset(dataset, input_dim=input_dim, samples=samples)
elif (dataset in ["fashion_mnist"]):
    x_train, y_train = NeuralNetwork.load_dataset(dataset, input_dim=input_dim, samples=samples)
elif (dataset in ["cifar10"]):
    x_train, y_train = NeuralNetwork.load_dataset(dataset, input_dim=input_dim, samples=samples)
else:
    quit()

# Learning Rate
learning_rate = 0.25

# Visualize images
NeuralNetwork.show_100_images(x_train)

# K Fold Validation Testing
kfold = StratifiedKFold(n_splits=kfolds, shuffle=True, random_state=1)
kfold_error = []
kfold_acc = []
for k, (train_index, val_index) in enumerate(kfold.split(x_train, y_train.argmax(1))):

    print ("\n========================\nK-fold: %d / %d\n========================" % (k+1,kfolds))

    # k-fold datasets
    x_train_fold, x_test_fold = x_train[train_index], x_train[val_index]
    y_train_fold, y_test_fold = y_train[train_index], y_train[val_index]

    # setup NN Model
    nn_dim = [input_dim*input_dim, input_dim*input_dim, 10]
    net = NeuralNetwork(nn_dim)
    net.set_speed(learning_rate)

    # optional settings
    net.set_init_method(net.init_rand)
    net.set_activation_function(net.activation_sigmoid)

    # Print to screen
    print ("\nNeural Network dimensions: %s" % nn_dim)
    print ("Dataset: %s" % dataset)
    print ("Image Dimensions: %d x %d px" % (input_dim,input_dim))
    print ("Training set size: %d" % len(x_train_fold))
    print ("Testing set size: %d" % len(x_test_fold))
    print ("Epochs: %d" % epochs)
    print ("Method: %s" % method)
    print ("Learning Rate (0-1): %5.4f\n" % net.get_speed())

    filename = "saved_models/model_%s_e%d_k%d.bin" % (dataset, epochs, k+1)

    if (method == "train"):

        # train
        print("Training...")
        net.train(x_train_fold, y_train_fold, x_test_fold, y_test_fold, epochs)

        # save to file
        net.save_to_bin(filename)

    else:

        # load from file
        print ("\nLoading model from BIN file...")
        net = NeuralNetwork.load_from_bin(filename)

    # Get error
    error = net.get_avg_error()
    kfold_error.append(error)
    print("\nError: %.5f" % (error))

    # Evaluate Accuracy
    acc = net.evaluate(x_test_fold, y_test_fold)
    kfold_acc.append(acc)
    print("Accuracy: %5.2f%%" % (acc * 100))

print ("\n========================\nK-fold Summary\n========================")
print("K-fold Accuracy: %5.2f%% +- %3.2f%%" % (np.mean(kfold_acc) * 100, np.std(kfold_acc) * 100))
print("K-fold Error:    %.3f +- %.3f" % (np.mean(kfold_error), np.std(kfold_error)))

print ("\nEnd of script.")
import pdb; pdb.set_trace()
