# An adaptive learning rate for back-propagation neural networks

## About

Author: LA Remmelzwaal

Written: January 2020

License: CC BY NC

Adapted from: {https://bitbucket.org/leenremm/python_neural_network

## Description

An adaptive learning rate for back-propagation neural networks

Adapted from: {https://bitbucket.org/leenremm/python_neural_network

Features added:

  * 3 alternative learning rates

## Software Installation

  * Install Python 3.6.4 or later: (e.g. https://www.python.org/ftp/python/3.6.4/python-3.6.4.exe) (25Mb)

## Virtual Environment Setup

  * Open Miniconda / Anaconda / MS Command Prompt
  * Run: `venv_setup.bat` to setup Virtual Env with PIP dependencies. This is only required once.

## Running the code

  * Activate virtual environment `venv_activate.bat`
  * Run: `python main.py`

## Datasets

![Example Datasets Images](https://bitbucket.org/leenremm/python_adaptive_learning_rate/raw/49143355c7d0ff1c4401d72f615be87ff7e55ad8/img/Fig01.png "Example Datasets Images")

## Baseline Accuracy

![Baseline Accuracy](https://bitbucket.org/leenremm/python_adaptive_learning_rate/raw/49143355c7d0ff1c4401d72f615be87ff7e55ad8/img/Fig04.png "Baseline Accuracy")

## Results of the Adaptive Learning Rate: MNIST

![Endline Accuracy](https://bitbucket.org/leenremm/python_adaptive_learning_rate/raw/49143355c7d0ff1c4401d72f615be87ff7e55ad8/img/Fig02.png "Endline Accuracy")

## Results of the Adaptive Learning Rate: Fashion MNIST

![Endline Accuracy](https://bitbucket.org/leenremm/python_adaptive_learning_rate/raw/49143355c7d0ff1c4401d72f615be87ff7e55ad8/img/Fig03.png "Endline Accuracy")
